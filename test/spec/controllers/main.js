'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('todoappApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  describe('model todos', function () {
    it('should have 0 todos to begin with', function () {
      expect(scope.todos.length).toBe(0);
    });
  });

  describe('addTodo', function () {
    it('should add a new todo to the list', function () {
      scope.addTodo('test');
      expect(scope.todos.length).toBe(1);
      expect(scope.todos[0].task).toBe('test');
      expect(scope.todos[0].done).toBe(false);
    });
  });

  describe('removeTodo', function () {
    it('should remove a the todo from the list', function () {
      scope.addTodo('test1');
      scope.addTodo('test2');
      scope.removeTodo(0);
      expect(scope.todos.length).toBe(1);
      expect(scope.todos[0].task).toBe("test2");      
    });
  });

  describe('doTodo', function () {
    it('should set the todo as done', function () {
      scope.addTodo('test1');
      scope.doTodo(0);
      expect(scope.todos[0].done).toBe(true);
    });
  });

});
