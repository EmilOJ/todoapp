'use strict';

angular.module('todoappApp')
  .controller('MainCtrl', function ($scope) {
    $scope.todos = [];
    
    $scope.addTodo = function (newToDoString) {
    	$scope.todos.push({task: newToDoString, done: false});
    };

    $scope.removeTodo = function (index) {
    	$scope.todos.splice(index, 1);
    };

    $scope.doTodo = function (index) {
    	$scope.todos[index].done = true;
    }
  });